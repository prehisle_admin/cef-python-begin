# An example of embedding CEF in wxPython application.

from cpb import cefpython
from pi_utils.path import get_app_path
from pi_utils.fancy_frame import FancyFrame

import wx

class CEFWindow:
    browser = None
    def __init__(self, url):
        windowInfo = cefpython.WindowInfo()
        windowInfo.SetAsChild(self.GetHandle())
        self.browser = cefpython.CreateBrowserSync(windowInfo,
                                                   browserSettings={},
                                                   navigateUrl=url)

        self.Bind(wx.EVT_SET_FOCUS, self.OnSetFocus)
        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.Bind(wx.EVT_IDLE, self.OnIdle)

    def OnSetFocus(self, event):
        cefpython.WindowUtils.OnSetFocus(self.GetHandle(), 0, 0, 0)

    def OnSize(self, event):
        cefpython.WindowUtils.OnSize(self.GetHandle(), 0, 0, 0)

    def OnClose(self, event):
        self.browser.CloseBrowser()
        self.Destroy()

    def OnIdle(self, event):
        cefpython.MessageLoopWork()

class CPBMainFrame(wx.Frame, CEFWindow):
    def __init__(self, title, url, icon="", size=(600,400)):
        wx.Frame.__init__(self, parent=None, id=wx.ID_ANY,
                          title=title, size=size)
        CEFWindow.__init__(self, url)
        if icon:
            ico = wx.Icon(icon, wx.BITMAP_TYPE_ICO)
            self.SetIcon(ico)


class CPBFancyFrame(FancyFrame, CEFWindow):
    def __init__(self, width, height, radius, url):
        FancyFrame.__init__(self, width, height, radius)
        CEFWindow.__init__(self, url)
        #self.SetTransparent( 220 )


if __name__ == '__main__':
    from cpb import UseDefaultSetting
    UseDefaultSetting()
    app = wx.App(False)
    CPBMainFrame('wxPython cef example', get_app_path("../demo/main_frame.html")).Show()
    ff = CPBFancyFrame(800, 600, 3, get_app_path("../demo/fancy_frame.html"))
    ff.Show()
    ff.Center()
    app.MainLoop()



