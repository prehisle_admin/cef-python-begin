#cef python begin

###依赖项目:
[git](http://git-scm.com)  
[python](http://www.python.org)  
[wxPython](http://www.wxpython.org)  
[CEF Python](https://code.google.com/p/cefpython)  


###环境搭建:
<https://msysgit.googlecode.com/files/Git-1.8.3-preview20130601.exe>  
<http://www.python.org/ftp/python/2.7.5/python-2.7.5.msi>  
<http://downloads.sourceforge.net/wxpython/wxPython2.8-win32-unicode-2.8.12.1-py27.exe>  
<http://cefpython.googlecode.com/files/cefpython3-27.3-win32-py27.exe>

###可选辅助项目
[coverage](https://pypi.python.org/pypi/coverage)
<https://pypi.python.org/packages/2.7/c/coverage/coverage-3.6.win32-py2.7.exe#md5=35a15698ee9637d10e17233c6767b279>


