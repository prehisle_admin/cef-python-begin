#!/usr/bin/env python
#-*- coding: utf-8 -*-
from cpb.frame import CPBFancyFrame, CPBMainFrame
from pi_utils.path import get_app_path
import wx

def main():
    from cpb import UseDefaultSetting
    UseDefaultSetting()
    app = wx.App(False)
    CPBMainFrame('wxPython cef example', get_app_path("main_frame.html"), get_app_path('icon.ico')).Show()
    ff = CPBFancyFrame(800, 600, 3, get_app_path("fancy_frame.html"))
    ff.Show()
    ff.Center()
    app.MainLoop()



if __name__ == '__main__': main()
