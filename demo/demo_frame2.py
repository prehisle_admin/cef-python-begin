#!/usr/bin/env python
#-*- coding: utf-8 -*-
from cpb.frame import CPBFancyFrame
from pi_utils.path import get_app_path
import wx
from cpb import cefpython

class DemoWebWindow(CPBFancyFrame):
    def __init__(self):
        CPBFancyFrame.__init__(self, 800, 600, 3, get_app_path("fancy_frame2.html"))
        bindings = cefpython.JavascriptBindings(bindToFrames=True, bindToPopups=True)
        bindings.SetObject("cef", self)
        self.browser.SetJavascriptBindings(bindings)
        self.bmove = False

    def TitleLeftDown(self):
        self.bmove = True
        #获取鼠标初始位置
        mouse=wx.GetMousePosition()
        #获取窗口初始位置
        frame=self.GetPosition()
        self.delta=wx.Point(mouse.x-frame.x,mouse.y-frame.y)

    def TitleMouseMove(self):
        if self.bmove == True:
            #获取鼠标新位置
            mouse=wx.GetMousePosition()
            #计算鼠标移动位置差值，Move窗体到相应位置
            self.Move((mouse.x-self.delta.x, \
                       mouse.y-self.delta.y))

    def TitleLeftUp(self):
        self.bmove = False

    def CefClose(self):
        self.OnClose(None)

    def MoveTo(self, x, y):
        pos = self.GetPosition()
        self.SetPosition( self.GetPosition() + (x, y))

def main():
    from cpb import UseDefaultSetting
    UseDefaultSetting()
    app = wx.App(False)
    ff = DemoWebWindow()
    ff.Show()
    ff.Center()
    app.MainLoop()



if __name__ == '__main__': main()